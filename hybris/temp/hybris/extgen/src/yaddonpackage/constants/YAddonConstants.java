/*
 * Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.newsproject.constants;

/**
 * Global class for all Newsaddon constants. You can add global constants for your extension into this class.
 */
@SuppressWarnings("deprecation")
public final class NewsaddonConstants extends GeneratedNewsaddonConstants
{
	public static final String EXTENSIONNAME = "newsaddon";

	private NewsaddonConstants()
	{
		//empty to avoid instantiating this constant class
	}

	// implement here constants used by this extension
}
