/*
 * Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.newsproject.constants;

/**
 * Global class for all Newsaddon web constants. You can add global constants for your extension into this class.
 */
public final class NewsaddonWebConstants // NOSONAR
{
	private NewsaddonWebConstants()
	{
		//empty to avoid instantiating this constant class
	}

	// implement here constants used by this extension
}
